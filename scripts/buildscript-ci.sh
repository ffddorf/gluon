#!/bin/bash -e
#Global variables that are needed to be passed:
#GLUONBRANCHES - The autoupdater branches to use: stable, beta and so on
#GLUONRELEASE - Release number to use for the images
#TAG - Git tag or branch 
#BUILDTARGETS - Comma seperated list of targets to build
#COMMUNITIES - Communities to build the firmware for
#VERBOSE - Set to 1 or 0 to enable or disable verbose building
#Optional variables:
#BUILD_NUMBER - Usually passed by CI system
#CI_COMMIT_SHA - Commit SHA of site / CICD git
#JOBS - Number of compiler threads
BUILD_NUMBER=${BUILD_NUMBER:=$(git rev-list --count HEAD)}
CI_COMMIT_SHA=${CI_COMMIT_SHA:=$(git rev-parse --verify HEAD)}
JOBS=${JOBS:=$(($(nproc --all) + 1))}

echo "---------------------------------------------------------------------------------------------------------"
echo "Starting firmware build at: $(date)"
echo "Gluon git tag: ${GLUONGITTAG}"
echo "CICD / Site config sha: ${CI_COMMIT_SHA}"
echo "CICD / Site config commit number: ${BUILD_NUMBER}"
echo "Gluon / Firmware release number will be: ${FIRMWARERELEASE}-${BUILD_NUMBER}"
echo "Gluon release branches to build: ${GLUONBRANCHES}"
echo "Gluon targets to build: ${GLUONTARGETS}"
[ "${VERBOSE}" = "true" ] && echo "Verbose mode is ON"
echo "Number of make jobs to run : ${JOBS}"
echo "---------------------------------------------------------------------------------------------------------"

#Clone git or fetch & checkout requested version
if [ -e "./gluon_build" ]; then
    (cd ./gluon_build && git fetch --all --tags && git checkout tags/${GLUONGITTAG})
else
    git clone --branch ${GLUONGITTAG} https://github.com/freifunk-gluon/gluon.git ./gluon_build
fi

#Create distribution / image destination folder
mkdir -p ./dist

#Link site & dist folders
[ -e "./gluon_build/site" ] || ln -s ../site ./gluon_build/site
[ -e "./gluon_build/dist" ] || ln -s ../dist ./gluon_build/dist

#Enter building directory
cd ./gluon_build

for GLUONBRANCH in ${GLUONBRANCHES}
do
    echo "Building ${GLUONBRANCH} images"
    #Delete old site config folder and output directory 
    rm -rf output
    
    #(Re)Create output & log directory
    mkdir -p output/logs

    echo "Running 'make update'"
    make update &> output/logs/mkupdate.log
    echo "Run make clean for all targets & rebuild toolchain if necessary"
    for TARGET in ${GLUONTARGETS}
    do
      if [ "${VERBOSE}" = "true" ]; then
        make clean GLUON_TARGET="${TARGET}" V=99 &> output/logs/clean.log
      else
        make clean GLUON_TARGET="${TARGET}" &> output/logs/clean.log
      fi
    done
    
    for TARGET in ${GLUONTARGETS}
    do
        echo "Building Gluon Target: ${TARGET}"
        if [ "${VERBOSE}" = "true" ]; then
            make GLUON_BRANCH="${GLUONBRANCH}" GLUON_TARGET="${TARGET}" GLUON_RELEASE="${FIRMWARERELEASE}-${BUILD_NUMBER}" -j${JOBS} V=99 &> output/logs/${TARGET}.log
        else
            make GLUON_BRANCH="${GLUONBRANCH}" GLUON_TARGET="${TARGET}" GLUON_RELEASE="${FIRMWARERELEASE}-${BUILD_NUMBER}" -j${JOBS} &> output/logs/${TARGET}.log
        fi
    done
    echo "Building manifest"
    make manifest GLUON_RELEASE="${FIRMWARERELEASE}-${BUILD_NUMBER}" GLUON_BRANCH="${GLUONBRANCH}"
    echo "Copying release folder contents"
    mkdir -p dist/${GLUONBRANCH}/${FIRMWARERELEASE}-${BUILD_NUMBER}
    mv output/images/* dist/${GLUONBRANCH}/${FIRMWARERELEASE}-${BUILD_NUMBER}
    mv output/logs dist/${GLUONBRANCH}/${FIRMWARERELEASE}-${BUILD_NUMBER}
    cp -Lr site dist/${GLUONBRANCH}/${FIRMWARERELEASE}-${BUILD_NUMBER} && rm -rf dist/${GLUONBRANCH}/${FIRMWARERELEASE}-${BUILD_NUMBER}/site/.git
    echo "Generating version.json"
    echo "{\"version\": \"${FIRMWARERELEASE}-${BUILD_NUMBER}\",\"tag\": \"${GLUONGITTAG}\"}" > dist/${GLUONBRANCH}/${FIRMWARERELEASE}-${BUILD_NUMBER}/version.json
    echo "${GLUONBRANCH} build done"
done
echo "Buildscript finished at $(date)"
